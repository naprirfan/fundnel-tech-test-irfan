<?php

namespace app\controllers;

use Yii;
use app\models\Post;
use app\models\PostSearch;
use app\models\Comment;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\ActiveRecord;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'update', 'create', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['?', '@'],
                    ],
                    [
                        'actions' => ['update', 'create', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Post model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $commentForm = new Comment();

        if ($commentForm->load(Yii::$app->request->post()) && !Yii::$app->user->isGuest) {

            $commentForm->post_id = $id;
            $commentForm->user_id = Yii::$app->user->id;
            $commentForm->author = Yii::$app->user->identity->username;

            if ($commentForm->save()) {
                return $this->redirect(['view', 'id' => $id]);
            }
        }

        return $this->render('view', [
            'model'       => $this->findModel($id),
            'commentForm' => $commentForm,
            'comments'    => Comment::find()->where(['post_id' => $id])->orderBy('create_time DESC')->all()
        ]);
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Post();

        if (Yii::$app->request->isPost) {

            $model->title = $_POST['Post']['title'];
            $model->description = $_POST['Post']['description'];
            $model->user_id = Yii::$app->user->id;
            $model->author = Yii::$app->user->identity->username;
            $model->imageFile = UploadedFile::getInstance($model, 'cover');

            //if uploading cover image..
            if (isset($model->imageFile->size) && $model->imageFile->size !== 0) {
                $model->cover = 'uploads/'. $model->imageFile->baseName . '.' . $model->imageFile->extension;    
            }

            if ($model->save()) {
                if (isset($model->imageFile->size) && $model->imageFile->size !== 0) {
                    $model->imageFile->saveAs('uploads/' . $model->imageFile->baseName . '.' . $model->imageFile->extension);
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        //the post doesn't belong to current user
        if ($model->user_id != Yii::$app->user->id) {
            throw new \yii\web\HttpException(403, 'The post belongs to someone else!');
        }

        if (Yii::$app->request->isPost) {

            $model->title = $_POST['Post']['title'];
            $model->description = $_POST['Post']['description'];
            $model->user_id = Yii::$app->user->id;
            $model->author = Yii::$app->user->identity->username;
            $model->imageFile = UploadedFile::getInstance($model, 'cover');

            //if uploading cover image..
            if (isset($model->imageFile->size) && $model->imageFile->size !== 0) {
                $model->cover = 'uploads/'. $model->imageFile->baseName . '.' . $model->imageFile->extension;    
            }
            
            if ($model->save()) {
                if (isset($model->imageFile->size) && $model->imageFile->size !== 0) {
                    $model->imageFile->saveAs('uploads/' . $model->imageFile->baseName . '.' . $model->imageFile->extension);
                }

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        //the post doesn't belong to current user
        if ($model->user_id != Yii::$app->user->id) {
            throw new \yii\web\HttpException(403, 'The post belongs to someone else!');
        }

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
}
