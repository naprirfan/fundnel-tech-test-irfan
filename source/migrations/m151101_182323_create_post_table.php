<?php

use yii\db\Schema;
use yii\db\Migration;

class m151101_182323_create_post_table extends Migration
{
    public function up()
    {
        $this->createTable('ftt_post', [
            'id'          => Schema::TYPE_PK,
            'user_id'     => Schema::TYPE_INTEGER. ' NOT NULL',
            'title'       => Schema::TYPE_STRING . '(128) NOT NULL',
            'description' => Schema::TYPE_STRING . '(128) NOT NULL',
            'author'      => Schema::TYPE_STRING . '(100) NOT NULL',
            'cover'       => Schema::TYPE_STRING . '(255) NOT NULL',
            'create_time' => Schema::TYPE_TIMESTAMP. ' NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'update_time' => Schema::TYPE_TIMESTAMP. ' NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ]);
    }

    public function down()
    {
        $this->dropTable('ftt_post');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
