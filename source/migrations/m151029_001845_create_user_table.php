<?php

use yii\db\Schema;
use yii\db\Migration;

class m151029_001845_create_user_table extends Migration
{
    public function up()
    {
        $this->createTable('ftt_user', [
            'id'          => Schema::TYPE_PK,
            'status'      => "ENUM('active','inactive') NOT NULL DEFAULT 'active'",
            'name'        => Schema::TYPE_STRING . '(100) NOT NULL',
            'email'       => Schema::TYPE_STRING . '(128) NOT NULL',
            'username'    => Schema::TYPE_STRING . '(32) NOT NULL',
            'password'    => 'CHAR(60) NOT NULL',
            'create_time' => Schema::TYPE_TIMESTAMP. ' NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'update_time' => Schema::TYPE_TIMESTAMP. ' NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ]);
    }

    public function down()
    {
        $this->dropTable('ftt_user');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
