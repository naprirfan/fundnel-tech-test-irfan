<?php

use yii\db\Schema;
use yii\db\Migration;

class m151101_182741_create_comment_table extends Migration
{
    public function up()
    {
        $this->createTable('ftt_comment', [
            'id'          => Schema::TYPE_PK,
            'post_id'     => Schema::TYPE_INTEGER. ' NOT NULL',
            'user_id'     => Schema::TYPE_INTEGER. ' NOT NULL',
            'author'      => Schema::TYPE_STRING . '(100) NOT NULL',
            'content'     => Schema::TYPE_STRING . '(128) NOT NULL',
            'create_time' => Schema::TYPE_TIMESTAMP. ' NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'update_time' => Schema::TYPE_TIMESTAMP. ' NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ]);
    }

    public function down()
    {
        $this->dropTable('ftt_comment');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
