<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Post */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if(isset($model->cover) && $model->cover != '') {
        echo Html::img('@web/' . $model->cover);
    }?>

    <p>
        <br />
        <?php if (!Yii::$app->user->isGuest && Yii::$app->user->id == $model->user_id) { ?>
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php } ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'author',
            'title',
            'description',
            'create_time',
        ],
    ]) ?>

    <?php if(!Yii::$app->user->isGuest) { ?>
    <?= $this->render('_comment_form', [
        'model' => $commentForm,
    ]) ?>
    <?php } ?>

    <h3>Comments</h3>

    <?php if(count($comments) == 0) : ?>
    <i>No Comments Yet</i>
    <?php endif; ?>
    
    <?php foreach($comments as $comment) :?>
        <div>
            <h4><?php echo $comment->author; ?></h4>
            <p>
                <?php echo $comment->content; ?> <i>(On <?php echo $comment->create_time; ?>)</i>&nbsp;
                <?php if (!Yii::$app->user->isGuest && Yii::$app->user->id == $comment->user_id) { 
                    echo Html::a('<span class="glyphicon glyphicon-trash"></span>', '/comment/delete/'. $comment->id, [
                        'title' => Yii::t('yii', 'Delete'),
                        'data-confirm' => Yii::t('yii', 'Are you sure to delete this item?'),
                        'data-method' => 'post',
                    ]);   
                }?>
            </p>
        </div>
        <div class="clearfix"></div>
    <?php endforeach; ?>
</div>
