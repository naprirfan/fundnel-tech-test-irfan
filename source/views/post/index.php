<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Posts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">
    <?php
        foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
          echo '<div class="alert alert-' . $key . '">' . $message . '</div>';
        }
    ?>

    <div class="jumbotron">
        <div class="container">
            <h1>Le Game Portal</h1>
            <p>This is a simple game portal web. You can add, update and review games here</p>
        </div>
    </div>
    
    <p>
        <?= Html::a(Yii::$app->user->isGuest ? 'Please Login To Create Post' : 'Create Post', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            'description',
            'author',
            'create_time',

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{view} {update} {delete}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        //modify visibility
                        if (!Yii::$app->user->isGuest && Yii::$app->user->id == $model->user_id) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('yii', 'Update'),
                            ]);       
                        }
                    },
                    'delete' => function ($url, $model) {
                        //modify visibility
                        if (!Yii::$app->user->isGuest && Yii::$app->user->id == $model->user_id) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => Yii::t('yii', 'Delete'),
                                'data-confirm' => Yii::t('yii', 'Are you sure to delete this item?'),
                                'data-method' => 'post',
                            ]);       
                        }
                    },
                ]
            ],
        ],
    ]); ?>

</div>
