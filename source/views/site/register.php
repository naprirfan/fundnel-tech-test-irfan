<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\User */
/* @var $form ActiveForm */
?>
<div class="register">

    <?php $form = ActiveForm::begin(); ?>
        
        <?= $form->field($model, 'name') ?>
        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'username') ?>
        <?= $form->Field($model, 'password')->passwordInput()->label('Password') ?>
        <?= $form->field($model, 'confirm_password')->passwordInput()->label('Confirm Password') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- register -->