<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ftt_comment".
 *
 * @property integer $id
 * @property integer $post_id
 * @property integer $user_id
 * @property string $author
 * @property string $content
 * @property string $create_time
 * @property string $update_time
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ftt_comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'user_id', 'author', 'content'], 'required'],
            [['post_id', 'user_id'], 'integer'],
            [['create_time', 'update_time'], 'safe'],
            [['author'], 'string', 'max' => 100],
            [['content'], 'string', 'max' => 128]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_id' => 'Post ID',
            'user_id' => 'User ID',
            'author' => 'Author',
            'content' => 'Add a comment',
            'create_time' => 'Create Time',
            'update_time' => 'Update Time',
        ];
    }
}
