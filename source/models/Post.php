<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "ftt_post".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property string $description
 * @property string $create_time
 * @property integer $update_time
 */
class Post extends ActiveRecord
{
    public $imageFile;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ftt_post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'title', 'description', 'author'], 'required'],
            [['user_id'], 'integer'],
            [['create_time', 'update_time', 'cover'], 'safe'],
            [['title', 'description'], 'string', 'max' => 128],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxSize' => 3072000, 'tooBig' => 'Limit is 3 MB']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'user_id'     => 'User ID',
            'author'      => 'Author',
            'title'       => 'Game Title',
            'description' => 'Game Description',
            'cover'       => 'Upload Game Cover',
            'create_time' => 'Create Time',
            'update_time' => 'Update Time',
        ];
    }

}
