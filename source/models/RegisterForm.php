<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\User;


class RegisterForm extends Model
{
    public $name;
    public $email;
    public $username;
    public $password;
    public $confirm_password;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string', 'min' => 2, 'max' => 100],

            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 32],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.'],
            ['email', 'string', 'min' => 2, 'max' => 128],

            ['password', 'required'],
            ['confirm_password', 'compare', 'compareAttribute' => 'password'],
            ['password', 'string', 'min' => 6],
            ['confirm_password','safe']
        ];
    }

    
}
