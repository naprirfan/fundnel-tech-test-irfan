#This is a simple game portal web. 

## Tested against PHP 5.5.30, MySQL 5.6.24, Apache 2.4.17

###Installation 

1. Clone this project into your chosen directory. Open your terminal, and type this : `git clone https://bitbucket.org/naprirfan/fundnel-tech-test-irfan.git`

0. Install PHP, MySql and Apache. Start all services and point your Apache's Document root to /source/web

1. Create new database with name : `fundnel` in MySQL. You can do this via http://localhost/adminer.php

2. Modify /source/config/db.php file. Change the following entry :
	- dsn : 'mysql:host=localhost;dbname=fundnel'
	- username : [your MySQL username]
	- password : [your MySQL password]

3. Go to /source and run migration command : `./yii migrate`

4. Go to http://localhost , Enjoy!

---

###Unit Testing

For unit testing installation guide, read `/source/tests/README.md`

Example of unit testing located at `/source/tests/unit/models/LoginFormTest.php`

To run test, go to `source/tests` and type `codecept run unit`

---

###Extras

Added SASS files to `/source/assets/sass` . You can compile those files by using compass : http://compass-style.org/install/


